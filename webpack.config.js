const HtmlWebPackPlugin = require("html-webpack-plugin");
const ServiceWorkerWebpackPlugin = require("serviceworker-webpack-plugin");
const WebpackPwaManifest = require('webpack-pwa-manifest');

const path = require('path');

const htmlWebpackPlugin = new HtmlWebPackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});

const serviceWorkerWebpackPlugin = new ServiceWorkerWebpackPlugin({
    entry: path.join(__dirname, '/src/sw.js'),
    excludes: ['**/.*', '**/*.map', '*.html'],
});

const pwaManifest = new WebpackPwaManifest({
    name: 'Connect',
    short_name: 'Conn',
    description: 'POC Garmin Connect app',
    background_color: '#ffffff',
    theme_color: "aliceblue",
    start_url: "/",
    display: "fullscreen",
    icons: [
        {
            src: path.resolve('src/assets/icon.png'),
            sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
        },
        {
            src: path.resolve('src/assets/large-icon.png'),
            size: '1024x1024' // you can also use the specifications pattern
        }
    ]
});

module.exports = {
    devtool: "#eval-source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                loader: [
                    'style-loader?sourceMap',
                    'css-loader',
                    'sass-loader']
            },
            {
                test: /\.css$/,
                use:['style-loader','css-loader']
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader'
                ]
            }
        ]
    },
    plugins: [htmlWebpackPlugin, serviceWorkerWebpackPlugin, pwaManifest]
};