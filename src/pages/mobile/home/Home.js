import React from 'react';
import Header from "../../../components/header/Header";

class MobileHome extends React.Component {

    render() {
        return (
            <div>
                <Header/>
                <div>Mobile page</div>
            </div>

        );
    }
}

export default MobileHome;