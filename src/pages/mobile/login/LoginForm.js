import React, {Component} from "react";
import { Link } from 'react-router-dom';
import {Button, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import BackgroundImage from '../../../assets/login.jpg';
import Logo from '../../../assets/garmin-connect-logo.png';
import './login.css';

export default class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        };
    }

    handleSubmit() {

    }

    render() {

        return (
            <div className="mainContainer" style={{background: `url(${BackgroundImage}) repeat center center fixed`}}>
                <div className="logoContainer">
                    <img src={Logo} className="logo"/>
                </div>
                <div className="well" className="wellStyles">
                    <form onSubmit={this.handleSubmit}>
                        <FormGroup controlId="email" bsSize="medium">
                            <ControlLabel style={{color: 'white', fontSize: '13px'}}>Email</ControlLabel>
                            <FormControl
                                autoFocus
                                type="email"
                                value={this.state.email}
                                onChange={e => this.setState({email: e.target.value})}
                            />
                        </FormGroup>
                        <FormGroup controlId="password" bsSize="medium">
                            <ControlLabel style={{color: 'white', fontSize: '13px'}}>Password</ControlLabel>
                            <FormControl
                                value={this.state.password}
                                onChange={e => this.setState({password: e.target.value})}
                                type="password"
                            />
                        </FormGroup>
                        <Button
                            block
                            bsSize="medium"
                            type="submit"
                            className="mainButtons"
                            style={{marginTop: '25px', color: 'white'}}
                        >
                            <Link to={"activities"}>Login</Link>
                        </Button>
                    </form>
                </div>
            </div>
        );
    }
}