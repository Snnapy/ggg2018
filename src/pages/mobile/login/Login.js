import React, {Component} from "react";
import {Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import BackgroundImage from '../../../assets/login.jpg';
import Logo from '../../../assets/garmin-connect-logo.png';
import './login.css';

class Login extends React.Component {

    render() {
        return (
            <div className="mainContainer" style={{background: `url(${BackgroundImage}) repeat center center fixed`}}>
                <div className="logoContainer">
                    <img src={Logo} className="logo"/>
                </div>
                <div className="well" className="wellStyles">
                    <Button bsStyle="primary" block className={"mainButtons"}>
                        Create Account
                    </Button>
                    <Button bsStyle="primary" block className={"mainButtons"} onClick={this.props.onSignInClicked}>
                        <Link to={"login"}>Sign In</Link>
                    </Button>
                    <div className="linkStyle">
                        <a href="#" style={{color: 'white'}}>Advanced</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;