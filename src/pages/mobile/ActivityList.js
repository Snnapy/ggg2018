import React from 'react';
import data from '../../data/activityList';

import '../../components/ActivityList/ActivityList.css';
import ActivityItem from '../../components/ActivityItem/ActivityItem';
import Header from "../../components/header/Header";

class ActivityList extends React.Component {

    render() {
        return (
            <div>
                <Header/>
                <div className='activity-list'>
                    {data.map((item, i) => {
                        return <ActivityItem activity={item} key={i} />
                    })}
                </div>
            </div>

        );
    }
}

export default ActivityList;