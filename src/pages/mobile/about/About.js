import React from 'react';
import Header from "../../../components/header/Header";

class MobileAbout extends React.Component {

    render() {
        return (
            <div>
                <Header/>
                <div>Mobile About page.</div>
            </div>

        );
    }
}

export default MobileAbout;