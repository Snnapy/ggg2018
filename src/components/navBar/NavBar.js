import React from 'react';
import './styles/NavBar.scss';
import Link from "react-router-dom/es/Link";

class Navigator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isExpanded:false
        };
        this.overlayClicked = this.overlayClicked.bind(this)
        this.menuExpanderClicked = this.menuExpanderClicked.bind(this)
    }

    overlayClicked (e){
        e.stopPropagation();
        this.setState({
            isExpanded:!this.state.isExpanded
        })
    }

   menuExpanderClicked (e){
        e.stopPropagation();
        this.setState({
            isExpanded:!this.state.isExpanded
        })
    }


    render() {
        return (
            <div>
                <nav role="navigation" className="navigation-bar">
                    <div id="menuToggle">
                        <input type="checkbox" checked={this.state.isExpanded}  onClick={(e)=>{
                            this.menuExpanderClicked(e);
                        }}/>

                        <div className="menu-overlay" onClick={(e)=>{
                            this.overlayClicked(e)
                        }}></div>

                        <span className="burger-line"></span>
                        <span className="burger-line"></span>
                        <span className="burger-line"></span>

                        <ul id="menu">
                            <li><Link to={`/GGG2018/activities`} activeclassname="active" className={"active"}><i className="fas fa-walking"></i><span>Activities</span></Link></li>
                            <li><Link to={`/GGG2018/health-status`} activeclassname="active" ><i className="fas fa-heartbeat"></i><span>Health Status</span></Link></li>
                            <li><Link to={`/GGG2018/performance`} activeclassname="active"><i className="fab fa-creative-commons-sampling"></i><span>Performance</span></Link></li>
                            <li><Link to={`/GGG2018/workouts`} activeclassname="active" ><i className="fas fa-stopwatch"></i><span>Workouts</span></Link></li>
                            <li><Link to={`/GGG2018/Courses`} activeclassname="active"><i className="fab fa-accessible-icon"></i><span>Courses</span></Link></li>
                            <li><Link to={`/GGG2018/Segments`} activeclassname="active" ><i className="fab fa-accessible-icon"></i><span>Segments</span></Link></li>
                            <li><Link to={`/GGG2018/Gear`} activeclassname="active" ><i className="fab fa-accessible-icon"></i><span>Gear</span></Link></li>
                            <li><Link to={`/GGG2018/Insights`} activeclassname="active" ><i className="far fa-lightbulb"></i><span>Insights</span></Link></li>
                            <hr/>
                            <li><Link to={`/GGG2018/Connections`} activeclassname="active" className={'social'} ><i className="fas fa-users"></i><span>Connections</span></Link></li>
                            <li><Link to={`/GGG2018/Groups`} activeclassname="active" className={'social'}><i className="fas fa-users"></i><span>Groups</span></Link></li>
                            <li><Link to={`/GGG2018/LiveTrack`} activeclassname="active" className={'social'}><i className="fas fa-eye"></i><span>LiveTrack</span></Link></li>
                            <hr/>
                            <li><Link to={`/GGG2018/download-course`} activeclassname="active" className={'green'} ><i class="fas fa-download"></i><span>Download golf Course</span></Link></li>
                            <li><Link to={`/GGG2018/garmin-course`} activeclassname="active" className={'green'} ><i class="fas fa-golf-ball"></i><span>Garmin Golf</span></Link></li>
                            <hr/>
                            <li><Link to={`/GGG2018/connect-store`} activeclassname="active" className={''} ><i className="fas fa-users"></i><span>ConnectIq Store</span></Link></li>
                            <hr/>
                            <li><Link to={`/GGG2018/devices`} activeclassname="active" className={'gray'} ><i class="fab fa-android"></i><span>Garmin Devices</span></Link></li>
                            <li><Link to={`/GGG2018/settings`} activeclassname="active" className={'gray'} ><i class="fas fa-cog"></i><span>Settings</span></Link></li>
                            <li><Link to={`/GGG2018/help`} activeclassname="active" className={'gray'} ><i class="fas fa-question"></i><span>Help</span></Link></li>
                        </ul>
                    </div>
                </nav>
            </div>

        );
    }
}

export default Navigator;