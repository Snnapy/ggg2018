import React from 'react';
import NavBar from "../navBar/NavBar";
import './styles/Header.scss';
class Header extends React.Component {


    render() {
        return (
            <div className={"header"}>
                <NavBar/>
                <div className={"next-to-nav-text"}>
                    <span className={"header-breadcrumb"}>All Activities</span>
               </div>
            </div>

        );
    }
}

export default Header;