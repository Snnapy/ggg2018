import React from 'react';
import cycling from '../../assets/images/cycling.png';
import floorclimbing from '../../assets/images/floor-climbing.png';
import multisport from '../../assets/images/multisport.png';
import other from '../../assets/images/other.png';
import running from '../../assets/images/running.png';
import strengthtraingin from '../../assets/images/strength-training.png';
import swimming from '../../assets/images/swimming.png';
import walking from '../../assets/images/walking.png';
import yoga from '../../assets/images/yoga.png';
import hiking from '../../assets/images/hiking.png';

const icons = {
    cycling: cycling,
    floor_climbing: floorclimbing,
    multisport: multisport,
    other: other,
    running: running,
    strength_training: strengthtraingin,
    swimming: swimming,
    walking: walking,
    yoga: yoga,
    hiking: hiking
};

import './ActivityItem.css';

class ActivityItem extends React.Component {
    render() {
        const {activity} = this.props;
        
        return (
            <div className='activity-item'>
                <img src={this.getImage()} alt={activity.activityName} title={activity.activityName} />
                <div className='activity-info'>
                    <a href="/" className='activity-name'>{activity.activityName}</a>
                    <p className='activity-date'>{this.formatDate(activity.startTimeLocal)}</p>
                    <p className='activity-stats'>
                        <span className='activity-duration'>{this.formatDuration(activity.duration)}</span>  <span className='activity-distance'>{this.formatDistance(activity.distance)}</span>
                    </p>
                </div>
            </div>
        );
    }
    
    getImage() {
        const {activity} = this.props;
        
        return icons[activity.activityType.typeKey];
    }

    formatDate(dateString) {
        const date = new Date(dateString);

        return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${this.padInt(date.getHours())}:${this.padInt(date.getMinutes())}`;
    }

    padInt(number) {
        if(number.toString().length === 1) {
            return '0' + number;
        }

        return number;
    }

    formatDuration(durationInSeconds) {
        let minutes = Math.floor(durationInSeconds / 60);
        let seconds = Math.round(durationInSeconds % 60);
        let hours = '';

        if(minutes > 60) {
            hours = Math.floor(minutes / 60);
            minutes = minutes % 60;
        }

        return `${hours ? hours + ':' : ''}${minutes > 0 ? this.padInt(minutes) : '00'}:${seconds > 0 ? this.padInt(seconds) : '00'}`
    }

    formatDistance(distance) {
        const km = distance / 1000;
        return km.toFixed(2) + ' km';
    }
}

export default ActivityItem;