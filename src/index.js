import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import runtime from 'serviceworker-webpack-plugin/lib/runtime';

import Home from "./pages/home/Home";
import About from "./pages/about/About";

import MobileHome from "./pages/mobile/home/Home";
import MobileAbout from "./pages/mobile/about/About";
import MobileLogin from "./pages/mobile/login/Login";
import LoginForm from "./pages/mobile/login/LoginForm";
import ActivityList from "./pages/mobile/ActivityList";

import './styles/main.scss';

if ('serviceWorker' in navigator) {
    const registration = runtime.register();
};

const isMobile =  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
let homePage = isMobile ? MobileHome : Home;
let aboutPage = isMobile ? MobileAbout : About;

const Routes = () => {
    return (
        <Router>
<div>
    <Route exact path="/GGG2018/" component={MobileLogin} />
    <Route path="/GGG2018/login" component={LoginForm} />
    <div className="page-container">
        <Route path="/GGG2018/activities" component={ActivityList} />
        <Route path="/GGG2018/home" component={MobileHome} />
        <Route path="/GGG2018/about" component={MobileAbout} />
    </div>
</div>

        </Router>
    );
};


ReactDOM.render(<Routes />, document.getElementById("container"));